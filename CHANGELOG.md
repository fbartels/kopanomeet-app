[1.0.0]
* Initial app release

[1.0.1]
* Fix directly calling users #3

[1.1.0]
* Enable Cloudron turn addon
* Update Kopano Meet to 2.1.0

[1.2.0]
* Enable guest access by default
  * create `/app/data/guests-disabled` to disable guest access

[1.2.1]
* Improve package meta data

[1.2.2]
* Update Kopano Meet to 2.2.3

