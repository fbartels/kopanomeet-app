This app packages KopanoMeet <upstream>2.2.3</upstream>

To introduce Meet in Cloudron there will be no user account limits in the beginning. An update that enforces user limits will be pushed in the second half of 2020.

## Meet In A Nutshell

Kopano Meet is a secure, open-source and easy-to-use solution for video conferencing. It is peer-to-peer and end-to-end encrypted which means that a conversation always takes place directly between the people in the call, with no other parties in between. Kopano Meet was developed as a progressive web app so interaction with Meet feels exactly the same, no matter the device that is used.

### Top Features

* One-on-one calls and video group meetings
* Guests can join group meetings with just one click on a link (if configured/enabled)
* Secure and easy sign-in your Cloudron account through Kopano Konnect
  * No need to create external accounts
* Hassle-free installation through Cloudron
  * Makes use of Cloudrons Turn server
* Screensharing directly from your browser

For more information on the features and capabilities, check out the Meet website: [https://meet-app.io/](https://meet-app.io/)

### Things you should know

* The Meet app in Cloudron offers the same functionalities as provided by the official [free, unsupported Meet package](https://meet-app.io/#pricing).
* Starting with Cloudron 5.1.0 Meet uses the TURN server provided through Cloudron.
* Want to upgrade to Meet Starter or Enterprise? Just buy [the desired package](https://meet-app.io/#pricing) and follow our upgrade instructions.

### Experiencing problems?

If you have problems establishing calls please create a new topic on the [Kopano forum](https://forum.kopano.io/category/23/kopano-meet-webmeetings). This topic should contain basic information like the Cloudron and Meet app version you are using as well as browsers and their versions. The Kopano team will gladly look at all reports that can be reproduced. If you have a Kopano subscription and need more assistance you can also reach out to the [Kopano support team](https://kopano.com/support-info/).

Please also make sure to check [our FAQ document](https://kopano.com/blog/top-10-things-to-ask-about-kopano-meet/).

### More Kopano products

Meet can be used as a standalone product, but can also be integrated into the Kopano groupware stack. [Let us know](mailto:sales@kopano.com?subject="I want Kopano on Cloudron!") if you also want to see an app for [Kopano Groupware](https://kopano.com/) on Cloudron.
