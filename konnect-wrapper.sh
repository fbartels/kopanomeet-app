#!/bin/bash

sed s/\ *=\ */=/g /app/data/konnectd.cfg > /tmp/konnectd-env
# shellcheck disable=SC2046
export $(grep -v '^#' /tmp/konnectd-env | xargs -d '\n')

exec /usr/sbin/kopano-konnectd serve